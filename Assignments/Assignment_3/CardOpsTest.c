/*
Darian Hadjiabadi
Test file for CardOps.c
*/
#include "CardOps.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
int main(void)
{
  
//Test acehigh and acelow
  setAceHigh();
  int ace;
  const char * aceTest = "Ace";
  ace = faceValue(aceTest);
  printf("Ace value has:%d\n",ace);
  
  setAceLow();
  ace = faceValue(aceTest);
  printf("Ace value has:%d\n",ace);
//Test faceValue
  int x;
  const char * test = "Six";
  x = faceValue(test);
  printf("face value:%d\n",x);
  test = "gfit";
  x = faceValue(test);
  printf("face value:%d\n",x);

  test = "2";
  x = faceValue(test);
  printf("face value:%d\n",x);
  
  test = "Ace";
  x = faceValue(test);
  printf("face value:%d\n",x);

//Test suitValue
  int y;
  const char * test2 = "SpAdes";
  y = suitValue(test2);
  printf("suit value of:%d\n",y);

  test2 = "Hello";
  y = suitValue(test2);
  printf("suit value of:%d\n",y);

  test2 = "DIAMONDS";
  y = suitValue(test2); 
  printf("suit value of:%d\n",y);

  test2 = "Hearts";
  y = suitValue(test2);
  printf("suit value of:%d\n",y);
//Test rank;
  const char * testsuit = "dIAMONDS";
  const int face = 12;
  int k;
  k = rank(testsuit,face);
  printf("rank of:%d\n",k);

  const char *test2suit = "clubs";
  const int face2 = 15;
  k = rank(test2suit,face2);
  printf("rank of:%d\n",k);

  const char *test3suit = "Anaconda";
  const int face3 = 1;
  k = rank(test3suit,face3);
  printf("rank of:%d\n",k);
  
  const char *test4suit = "Hearts";
  const int face4 = 10;
  k = rank(test4suit,face4);
  printf("rank of:%d\n",k); 


//Test parce
  int z;
  char * suit = malloc(sizeof(char));
  int * facePtr = malloc(sizeof(int));
  const  char * cardstring = "King of Spades";
  z = parse(cardstring,suit,facePtr);
  printf("After parcing, rank of:%d\n",z);
  cardstring = "Too of You";
  z = parse(cardstring,suit,facePtr);
  printf("After parcing, rank of:%d\n",z);
  free(suit); free(facePtr);
  
  return 0; 
}
