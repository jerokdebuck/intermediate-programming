/* 
Darian Hadjiabadi
600.120 
Due 06/24/13
pg3b
772 559 1911
dhadjia1
dhadjia1@gmail.com
*/
#include <stdio.h>
#include <strings.h>
#include <stdlib.h>
#include "CardOps.h"
#include <string.h>

struct cardGames{char * cardString; int  faceValue;char * suit; int rank;};
int lineNum = 0;

//Note this functions doesn't fully work
void eliminateEquals(struct cardGames *c)
{
  int look;
  int j = 0;
  for(look = 1; look < lineNum; look++)
  {
    if( ((c+look) -> faceValue) != ((c+j) -> faceValue) ||  (c+look) -> suit != (c+j) -> suit)
    {
       j++;
       (c+j) -> suit = (c+look) -> suit;
       (c+j) -> faceValue = (c+look) -> faceValue;
       strcpy((c+j) -> cardString,(c+j) -> cardString);       
       (c+j) -> rank = (c+look) -> rank;
    }
  }    
}
void decreaseSort(struct cardGames *c)
{
//Idea obtained from www.tenouk.com/cpluscodesnippet/sortarrayelementsasc.ht//ml
  int pass =1; 
  int i;
  int holdRank;
  char* holdSuit;
  int holdFace;
  char * holdString;
  
  for(pass = 1; pass <= (lineNum -1); pass++)
  {
      for(i = 0; i <= (lineNum-2); i++)
      {
        if((c+i+1)->rank > (c+i) -> rank)
        {
          holdRank = (c+i+1) -> rank;
          holdSuit = (c+i+1) -> suit;
          holdFace = (c+i+1) -> faceValue;
          holdString = (c+i+1) -> cardString;
          (c+i+1) -> rank = (c+i) -> rank;
          (c+i+1) -> suit = (c+i) -> suit;
          (c+i+1) -> faceValue = (c+i) -> faceValue;
          (c+i+1) -> cardString = (c+i) -> cardString;
          (c+i) -> rank = holdRank;
          (c+i) -> suit = holdSuit;
          (c+i) -> faceValue = holdFace;
          (c+i) -> cardString = holdString;
                  
        }
     }
   } 
} 

  
int main(int argc, char * argv[])
//int main(void)
{
  if(argc ==  1)
    printf("%s\n",argv[0]);
 else
 {
    int size = 5;
    struct cardGames * cardarray = malloc(size*sizeof(struct cardGames));


    FILE * fptr;
   // fptr = fopen("cardstring.txt","r");
    fptr = fopen(argv[1],"r");
    char string[50];
    int nul;
    for(nul = 0; nul < 50; nul++)
      string[nul] = '\0';

    if(fgets(string,50,fptr) != NULL){} 
 
 while(!feof(fptr))
 {  
      char * isSuit = malloc(sizeof(char));
      int isFace = 0;
      int ranker = 0;
      if(lineNum % 5 == 4)
      {   
        size *= 2;
        cardarray = realloc(cardarray,size*sizeof(struct cardGames));
      }
      ranker = parse(string,isSuit,&isFace);
 //Put variables into struct parameters
 
      (cardarray+lineNum) -> suit = malloc(strlen(isSuit)+1);
      (cardarray+lineNum) -> cardString = malloc(strlen(string)+1);
      (cardarray+lineNum)-> faceValue = isFace; 
      strcpy((cardarray+lineNum)->cardString,string);
      (cardarray+lineNum)->rank = ranker;
      strcpy((cardarray+lineNum)->suit,isSuit); 
      lineNum++;
      if(fgets(string,50,fptr) != NULL) {} 
      free(isSuit);
    }  
    decreaseSort(cardarray);
    eliminateEquals(cardarray);
    int k;  
//note that if output contains '(null) has rank of 0', it means 
//eliminateEquals took it out
    for(k = 0; k < (lineNum-1); k++)
      printf("%s has rank of %d\n",(cardarray+k)->cardString,(cardarray+k)->rank);
    
                 
    for(k = 0; k < lineNum; k++)
     {
        free((cardarray+k)->suit);
        free((cardarray+k)->cardString);
     }  
    free(cardarray);
    fclose(fptr);
 }  
    return 0;
}
/*Reflections: This program all together took me around 8-10 hours 
to complete. I definately feel a bit more comfortable with 
using pointers and structures.  */
