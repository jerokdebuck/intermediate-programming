#include <stdio.h>
#include <strings.h>
#include <stdlib.h>
#include "CardOps.h"
#include <ctype.h>

static int ace_value = 14;
static const char * SUITS[4] = {"Diamonds","Clubs","Hearts","Spades"};

void setAceHigh()
{
  ace_value = 14;
}  
void setAceLow()
{
 ace_value = 1;
}
int suitValue(const char* suitPtr)
{ 
  int i;
  for(i = 1; i <= 4; i++)
  {
    if(strcasecmp(suitPtr,SUITS[i-1]) == 0)
    {
        return i;
    }
  }
    return 0;
}
int faceValue(const char* faceStr)
{ 
 
  int i;
  const char *values[14] = {"One","Two","Three","Four","Five","Six","Seven","Eight","Nine","Ten","Jack","Queen","King","Ace"};
  const char *numbervals[14] = {"1","2","3","4","5","6","7","8","9","10","beep","beep","beep","beep"};
  
  for(i = 1; i <= 14; i ++)
  {
    if(strcasecmp(faceStr,values[i-1]) == 0 || strcasecmp(faceStr,numbervals[i-1]) == 0)
    {
      if(i == 14)
        return ace_value;
      else
        return i;
    }
  } 
  return 0;
}
int rank(const char* suit,const int face)
{
  if(face > 14 || face < 0)
    return 0;
  else
    return 13*(suitValue(suit)) + face;
}
int parse(const char* cardString, char* suit, int* facePtr)
{
   char * value = malloc(sizeof(char));
   if(sscanf(cardString,"%s of %s",value,suit) == 1) {}
   *facePtr = faceValue(value);
    free(value);
   return rank(suit,*facePtr);
}

