
//static int ace_value = 14;
//static const char * SUITS[4] = {"Diamonds","Clubs","Hearts","Spades"};
#ifndef CARDOPS_H
#define CARDOPS_H
void setAceHigh();
void setAceLow();
int suitValue(const char* suitPtr);
int faceValue(const char* faceStr);
int rank(const char* suit, const int face);
int parse(const char* cardString, char* suit, int* facePtr);

#endif

