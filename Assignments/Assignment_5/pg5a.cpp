/*
Darian Hadjiabadi
600.120 Assignment 5 due Jun 18
dhadjia1 
dhadjia1@gmail.com
*/
#include <iostream>
#include <string>
#include <vector>
#include "PhoneData.h" 
#include "function.h"
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <list>
#include <algorithm>


using std::cout;
using std::endl;
using std::cin;
using std::string;

//Initialzie vectors for regular and favorite contacts
std::vector<Contact>regs;
std::vector<Contact>favs;
static int count_fav = -1;
static int count_reg = -1;


int main(void)
{
  opening_menu();
  return 0;
} 

void opening_menu()
{
  const string option_A = "a) See Favorites";
  const string option_B = "b) See Contacts";
  const string option_C = "c) Make Changes";
  const string option_D = "d) Find contact";
  const string option_E = "e) Rearrange a Favorite";
  const string menu = "Menu: Please Enter One";
 
  const string spaces(menu.size()+3,' ');
  const string covering(spaces.size(),'*');
  
  cout<<covering<<endl; 
  cout<<" " <<menu<<" "<<endl;
  cout<<" "<<option_A<<" "<<endl;
  cout<<" "<<option_B<<" "<<endl;
  cout<<" "<<option_C<<" "<<endl;
  cout<<" "<<option_D<<" "<<endl;
  cout<<" "<<option_E<<" "<<endl;
  cout<<covering<<endl;
  cout<<"Please choose a,b,c,d, or e: ";
  char next;
  cin>>next;
  if(next == 'c')
    update_menu(); //can add, edit, remvoe
  else if(next == 'b')
    disp_regular(); //can display regular contacts
  else if(next == 'a')
    disp_favorite(); //can display favorites
  else if(next == 'd')
    find_contact(); //can find a particular contact
  else if(next == 'e')
    rearrange(); //can rearrange a contact in the fav list
}

void disp_favorite()
{
  cout<<"Would you like to see in alphabetical order?"<<endl;
  cout<<"Type 'yes' for yes or 'no' for no: ";
  string decision;
  cin>>decision;
  if(decision == "no")
  {
    int i;
    for(i = 0; i <= count_fav; i++) //output contacts
    {
      cout<<favs[i].name<<endl;
      cout<<favs[i].type_home<<": "<<favs[i].number_home<<endl;
    
      cout<<favs[i].type_cell<<": "<<favs[i].number_cell<<endl;
      cout<<favs[i].type_work<<": "<<favs[i].number_work<<endl;
    cout<<favs[i].type_fax<<": "<<favs[i].number_fax<<endl;
    cout<<endl;
    }
  }
  else if(decision == "yes")
  {
   std::sort(favs.begin(),favs.end(),compare_word); 
    //sorts in alphabetical order
   int i;
   for(i = 0; i<=count_fav;i++)
   {
    cout<<favs[i].name<<endl;
    cout<<favs[i].type_home<<": "<<favs[i].number_home<<endl;
    cout<<favs[i].type_cell<<": "<<favs[i].number_cell<<endl;
    cout<<favs[i].type_work<<": "<<favs[i].number_work<<endl;
    cout<<favs[i].type_fax<<": "<<favs[i].number_fax<<endl;

   }

  }
   

    cout<<"Type 'back' to go back or ^d to quit: ";
    string decision2;
    cin>>decision2;
  if(decision2 == "back")
    opening_menu();
     
}

void disp_regular()
{
  cout<<"Would you like to display in alphabetical order?"<<endl;
  cout<<"Type 'yes' for yes or 'no' for no: ";
  string alphabetical;
  cin>>alphabetical;
  if(alphabetical == "no")
  {
    int i;
    for(i =0; i <= count_reg; i++) //outputs regular list
    {
    cout<<regs[i].name<<endl;
    cout<<regs[i].type_home<<": "<<regs[i].number_home<<endl;
    cout<<regs[i].type_cell<<": "<<regs[i].number_cell<<endl;
    cout<<regs[i].type_work<<": "<<regs[i].number_work<<endl;
    cout<<regs[i].type_fax<<": "<<regs[i].number_fax<<endl;
    cout<<endl;
    }
  }
  else if(alphabetical == "yes")
  {
    std::sort(regs.begin(),regs.end(),compare_word);
    //sorts in alphabetical order
    int i;
    for(i = 0; i <= count_reg; i++)
    {
      cout<<regs[i].name<<endl;    
      cout<<regs[i].type_home<<": "<<regs[i].number_home<<endl;
      cout<<regs[i].type_cell<<": "<<regs[i].number_cell<<endl;
      cout<<regs[i].type_work<<": "<<regs[i].number_work<<endl;
      cout<<regs[i].type_fax<<": "<<regs[i].number_fax<<endl;
      cout<<endl;
   }

  }
  cout<<"Type 'back' to go back or ^d to quit: ";
  string decision;
  cin>>decision;
  if(decision == "back")
    opening_menu();

}

void update_menu()
{
  cout<<endl;
  const string menu = "Make Changes: Please Choose One";
  const string option_A = "a) Add";
  const string option_B = "b) Remove";
  const string option_C = "c) Edit";
  const string covering(menu.size()+3,'*');

  cout<<covering<<endl;
  cout<<" "<<menu<<" "<<endl;
  cout<< " "<<option_A<<" "<<endl;   
  cout<< " "<<option_B<<" "<<endl;  
  cout<< " "<<option_C<<" "<<endl;
  cout<<covering<<endl;  

  cout<<"Please choose a, b, or c. OR enter 'back' to go back or ^d to quit: ";
  string choice;
  cin>>choice;  
  if(choice == "back")
    opening_menu();
  else if(choice == "a")
    add_menu();
  else if(choice == "b")
    remove_contact();
  else if(choice == "c")
    edit_contact();

}

void add_menu() //add a new contact
{
  cout<<"Enter 'f' to add to Favorite or 'r' to add to Regular: ";
  char where;
  cin>>where;  
  if(where == 'f')
    count_fav++;
  else if(where == 'r')
    count_reg++; 
  if(where != 'f' && where != 'r')
  {
    cout<<"Invalid choice"<<endl;
    add_menu(); 
  }
  string name_add;
  cout<<"Please enter name: ";
  cin>>name_add;
  string base =  "Please enter type-->'h' for home, 'c' for cell, 'w' for work, 'f' for fax: ";
 
/* if the person makes a mistake, they can change the number again */
  string cont = "yes";
  while(cont == "yes") 
  //user can input numbers for all number pairs
  {
      cout<<base;
      char type;
      cin>>type;
      string phone_number; 
      cout<<"Please enter the number: ";
      cin>>phone_number;
      while(phone_number.size() != 10)
      {
        cout<<"Invalid number, make sure it is 10 digits long and has no spaces"<<endl;
        cout<<"Please re-enter number: ";
        cin>>phone_number;
      } 
       
      if(type == 'h')
        cout<<"Home added"<<endl; 
      else if(type == 'w')
        cout<<"Work added"<<endl;
      else if(type == 'c')
        cout<<"Cell added"<<endl;
      else if(type == 'f')
        cout<<"Fax added"<<endl;
      else
      {
        cout<<"Error, invalid choice, please start over";
        add_menu();
      }
      cout<<"Continue? y/n: ";
      cin>>cont;
      if(cont == "yes" || cont =="y")
      {
        add_contact_number(type,where,phone_number);
        cont = "yes";
      }
      else
      {
        add_contact_number(type,where,phone_number);
        add_contact_name(where,name_add);
        break;
      }
    
   }
   update_menu();
}

void add_contact_number(char c,char w,string number)
{
  //just puts number in appropriate places
  string location[4] = {"HOME","CELL","WORK","FAX"};
  string choose;
  if(w == 'r')
    regs.push_back(Contact()); //creates a new regular 
  else if(w == 'f')
    favs.push_back(Contact()); //creates a new favorite

  if(c == 'h')
  {
    choose = location[0];
    if(w == 'r')
    {
      regs[count_reg].type_home = choose;
      regs[count_reg].number_home = number;
    }
    else if(w=='f')
    {
      favs[count_fav].type_home = choose; 
      favs[count_fav].number_home = number;
    }
  }
  else if(c == 'c')
  {
     choose = location[1];
     if(w == 'r')
     {
      regs[count_reg].type_cell = choose;
      regs[count_reg].number_cell = number;
     }
     else if(w == 'f')
     {
      favs[count_fav].type_cell = choose;
      favs[count_fav].number_cell = number;
     }
  }
  else if(c == 'w')
  {
     choose = location[2];
     if(w == 'r')
     {
        regs[count_reg].type_work = choose;
        regs[count_reg].number_work = number;
     }
     else if(w == 'f')
     {
       favs[count_fav].type_work = choose;
       favs[count_fav].number_work = number;
     }
  }
  else if(c == 'f')
  {
    choose = location[3];
    if(w == 'r')
    {
      regs[count_reg].type_fax = choose;
      regs[count_reg].number_fax = number;
    }
    else if(w == 'f')
    {
      favs[count_fav].type_fax = choose;
      favs[count_fav].number_fax = number;
    }
  }
}

void add_contact_name(char w, string namer)
{
  if(w == 'r')
    regs[count_reg].name = namer;
  if(w == 'f')
    favs[count_fav].name = namer; 
}

void remove_contact()
{
 //Not really 100% complete. 
  cout<<"Name: ";
  string named;
  cin>>named;
  std::vector<Contact>::iterator v;
  for(v = regs.begin(); v != regs.end(); v++)
  {
    if(v->name == named)
    {
      regs.erase(v,v+1);
      opening_menu();
    }
  }
  for(v = favs.begin(); v!=favs.end(); v++)
  {
    if(v->name == named)
    {
      favs.erase(v,v+1);
      opening_menu();
    }
  }
  cout<<"Did not find: "<<named<<endl;
  opening_menu();
}
void edit_contact()
{
  //Can edit the name or a number pair
  cout<<"Favorite (type 'f') or regular(type 'r')?: ";
  char c;
  cin>>c;
  cout<<"Which contact would you like to edit?: ";
  string named;
  cin>>named;
  std::vector<Contact>::iterator v;
  if(c == 'r')
  { 
    for(v = regs.begin();v!=regs.end(); v++)
    {
      if(v->name == named)
      {
        cout<<"Edit name(type 'n') or number pair (type 'p'): ?";
        char edit;
        cin>>edit;
        if(edit == 'n')
        {
          cout<<"Input new name: ";
          string new_name;
          cin>>new_name;
          v->name = new_name;
        }
        else if(edit =='p')
        {
          cout<<"Edit cell(type 'c'), home (type 'h'),work(type 'w') or fax(type 'f')?: ";
          char type;
          cin>>type;
          cout<<"Enter new number: ";
          string new_number;
          cin>>new_number;
          if(type == 'c')
            v->number_cell = new_number;
          else if(type == 'h')
            v->number_home = new_number;
          else if(type == 'w')
            v->number_work = new_number;
          else if(type == 'f')
            v->number_fax = new_number;
        }
      }
    }
    cout<<"Editing complete!"<<endl;
    opening_menu();
  }
  
  if(c == 'f')
  {
    for(v = favs.begin();v!=favs.end(); v++)
    {
      if(v->name == named)
      {
        cout<<"Edit name(type 'n') or number pair (type 'p'): ?";
        char edit;
        cin>>edit;
        if(edit == 'n')
        {
          cout<<"Input new name: ";
          string new_name;
          cin>>new_name;
          v->name = new_name;
        }
        else if(edit =='p')
        {
          cout<<"Edit cell(type 'c'), home (type 'h'),work(type 'w') or fax(type 'f')?: ";
          char type;
          cin>>type;
          cout<<"Enter new number: ";
          string new_number;
          cin>>new_number;
          if(type == 'c')
          {
            v->number_cell = new_number;
            v->type_cell = "CELL";
          }
          else if(type == 'h')
          {
            v->number_home = new_number;
            v->type_home = "HOME";
          }
          else if(type == 'w')
          {
            v->number_work = new_number;
            v->type_work = "WORK";
          }
          else if(type == 'f')
          {
            v->number_fax = new_number;
            v->type_fax = "FAX";
          }
        }
      }
    }
    cout<<"Editing complete!"<<endl;
    opening_menu();
  }
  


}
  
void find_contact()
{ //can find a contact and display their numbers
  cout<<"Enter name of contact you wish to find: ";
  string named;
  cin>>named;
  std::vector<Contact>::iterator v;
  for(v = regs.begin();v != regs.end(); v++)
  {
    if(v->name == named)
    {
      cout<<"Here is the contact!"<<endl;
      cout<<v->name<<endl;
      cout<<v->type_home<<": "<<v->number_home<<endl; 
      cout<<v->type_cell<<": "<<v->number_cell<<endl;
      cout<<v->type_work<<": "<<v->number_work<<endl;
      cout<<v->type_fax<<": "<<v->number_fax<<endl;
      opening_menu();
    }
  }
  for(v = favs.begin(); v!= favs.end(); v++)
  {
    if(v->name == named)
    {
      cout<<"Here is the contact!"<<endl; 
      cout<<v->type_home<<": "<<v->number_home<<endl;
      cout<<v->type_cell<<": "<<v->number_cell<<endl;
      cout<<v->type_work<<": "<<v->number_work<<endl;
      cout<<v->type_fax<<": "<<v->number_fax<<endl;
      opening_menu();
    }
  }

}

bool compare_word(const Contact &x,const Contact &y)
{
  //compares if one word is higher on the alphabet 
  // than another. 
  return x.name < y.name;  

}

void rearrange()
{
  //rearranges favorite list
  cout<<"Enter name of contact: ";
  string named;
  cin>>named;
  cout<<"There are currently: "<<favs.size()<<" entries. Where would you like to put: "<<named<<"? Please enter a position number: ";
  unsigned int pos;
  cin>>pos;
  if(pos > favs.size())
  {
    cout<<"Error, invalid choice";
    opening_menu();
  }
  int i = 0;
  std::vector<Contact>::iterator v;
  for(v = favs.begin(); v != favs.end(); v++)
  {
    if(v-> name == named)
    {
      std::swap(favs[i],favs[pos-1]);
      opening_menu();
    }
    else
      i++;
  }
  cout<<"Name not found"<<endl;
  opening_menu();
}


/*REFLECTIONS: This program required a lot of output simply
because the assingment called for it. I feel like that is 
one of the reasons why it was so long. I probably could havemade an output function for some of the parts and saved a decent amount of lines. I got a bit more comofrtable with vectors and working with structures overall. I would say this took about 8 hours */
