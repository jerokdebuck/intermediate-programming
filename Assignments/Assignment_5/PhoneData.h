/*Darian Hadjiabadi PhoneData.h for assignment 5 */

#ifndef PHONEDATA_H
#define PHONEDATA_H
#include <string>
#include <vector>

using std::string;

struct Contact
{
  string name;
  string type_home;
  string type_cell;
  string type_work;
  string type_fax;
  string number_home;
  string number_cell;
  string number_work;
  string number_fax;
};

#endif //_PHONEDATA_H
