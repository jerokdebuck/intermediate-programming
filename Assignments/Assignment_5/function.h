/* Darian Hadjiabadi function.h for assignmtnet 5
*/

#ifndef FUNCTION_H
#define FUNCTION_H

#include<vector>

void opening_menu();
void update_menu();
void add_menu();
void edit_menu();
void remove_menu();
void disp_favorite();
void disp_regular();
void add_contact_number(char,char,string);
void add_contact_name(char,string);
void edit_contact();
void remove_contact();
void find_contact();
bool compare_word(const Contact &,const Contact &);
void rearrange();

#endif //FUNCTION_H
