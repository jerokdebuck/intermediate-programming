/*
 Darian Hadjiabadi
600.120
Assignment 1 pg1b due Tu June 4, 2013
772-559-1911
dhadjia1
dhadjia1@gmail.com
*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

/*random_color detects which triplet is being processed into order to 
return the correct R,G,or B value for the new color */
int random_color = 1; 
unsigned char code1;
unsigned char code2; 
unsigned char code3; 

/*inttoBin gives the binary representation of each integer
bintoRGB processes the bits of the binary sequences and gives back the 
rgb(x,y,z) code
rgbtoInt takes an rgb code back into its original 32-bit value
arraytoInt takes a char array and turns it into an unsigned integer
(i.e) [1,2,3,4] -> the number 1234 */

unsigned char inttoBin(unsigned int);
unsigned char bintoRGB(char []);
unsigned int rgbtoInt(unsigned char,unsigned char,unsigned char); 
unsigned int arraytoInt(unsigned char [], int);

int main(void)
{ 
  
  unsigned int input1; 
  unsigned int input2;
  unsigned int input3;  
  char c; 
  int i,x;
  /* i is used to see which value array the input will go into 
  whereas x is the index of each value array */

 unsigned char value1[11],value2[11],value3[11];

  /*First of the triplet goes into value1, second input goes into value2
   third input goes into value3
  */
  
  /* fill the array with 0's */
  for(i =0; i < 11; ++i)
  {
    value1[i]='0'; 
    value2[i]='0';
    value3[i]='0';
  }
  value1[10] = '\0';
  value2[10] = '\0';
  value3[10] = '\0';
  printf("Input three numbers in range[0,2^32-1]. Please ^d to exit \n");   
  i = 0;  
  c = getchar(); 
  x = 0;
  printf("<html>\n");
  printf("<table>\n");
/*Reads in the triplet and places each into seperate char arrays */
  while(c != EOF)
  {
    if(!isdigit(c))
    {
	printf("Error, you did not input an integer\n");
	return 0;
    }   
    if(i == 0 && !isspace(c))
    {
      value1[x] = c;
      x++; 
      c = getchar(); 
    }
    else if(i == 1 && !isspace(c))
    { 
      value2[x] = c;
      x++; 
      c = getchar(); 
    }
    else if(i == 2 && !isspace(c))
    { 
      value3[x] = c;
      x++;  
      c = getchar(); 
    }
    while(isspace(c)  && i == 0)
    {
      c = getchar();
      if(!isspace(c))
      {
        value1[x++] = '\0';
        i++;
        x = 0; 
      }

    }
    /* Implement 'while' loops to skip all white spaces*/

    while(isspace(c) && i == 1)
    {
      c = getchar(); 
      if(!isspace(c))
      {
        value2[x++] = '\0';
        x = 0;
        i++;        
      }

    }
    while(isspace(c) && i ==2)
    {
      c = getchar();
      if(!isspace(c))
      {
        value3[x++] = '\0';
        x=0;
        ++i;
      }
    }
  /*Once all three numbers have been read in, continue */
    if(i == 3)
    {
    /*Checks to see if negative */
      i = 0; 
      printf("<tr>\n");
      input1 = arraytoInt(value1,10);
      input2 = arraytoInt(value2,10);
      input3 = arraytoInt(value3,10);
      code1 = inttoBin(input1);
      random_color = 2;
      code2 = inttoBin(input2);
      random_color = 3;
      code3 = inttoBin(input3);
      unsigned int integer_mix;
      integer_mix = rgbtoInt(code1,code2,code3);
      printf("<td bgcolor=\"%x\">\n",~input1);
      printf("<font color=\"%x\">#%06x</font>\n",input1,input1);
      printf("<td bgcolor=\"%x\">\n",~input2);
      printf("<font color=\"#%x\">#%06x</font>\n",input2,input2);
      printf("<td bgcolor=\"%x\">\n",~input3);
      printf("<font color=\"#%x\">#%06x</font>\n",input3,input3);
      printf("<td bgcolor=\"%x\">\n",~integer_mix);
      printf("<font color=\"#%x\">#%06x</font>\n",integer_mix,integer_mix);
      printf("</tr>\n");
      c = getchar();
      while(isspace(c))
		c = getchar(); 
    }
	
  }
    printf("</table>\n");
    printf("</html>\n");
    printf("</table>\n");
    return 0;
}

unsigned int rgbtoInt(unsigned char code1, unsigned char code2, unsigned char code3)
{
  char bit_holder1[9];
  char bit_holder2[9];
  char bit_holder3[9];
  char total_holder[33];
  int zeros;
  for(zeros = 0; zeros < 8; ++zeros)
  {
    bit_holder1[zeros] = '0';
    bit_holder2[zeros] = '0';
    bit_holder3[zeros] = '0';
  }
  for(zeros = 0; zeros < 32; ++zeros)
    total_holder[zeros] = '0'; 
  
  bit_holder1[8] = '\0';
  bit_holder2[8] = '\0';
  bit_holder3[8] = '\0';  
  total_holder[32] = '\0';
/* Gets the bits for each r,b,g value */
  int f;
  int g;
  int h; 
  int a;
  int b;
  int c;
  int i; 
  i = 0;
  for(f = 7;f>= 0; f--)
  {
    a = code1 >> f;
    if(a & 1)
      bit_holder1[i++] = '1';
    else
      bit_holder1[i++] = '0';
  }
  i = 0;
  for(g = 7; g>=0;g--)
  {
    b = code2 >> g;
    if(b & 1)
      bit_holder2[i++] = '1';
    else
      bit_holder2[i++] = '0';
  }
  i = 0;
  for(h = 7; h>= 0; h--)
  {
    c = code3 >> h;
    if(c&1)
      bit_holder3[i++] = '1';
    else
      bit_holder3[i++] = '0';
  }
  /*puts the bits into a size 32 char array */
  for(i = 0; i < 8; ++i)
    total_holder[i] = '0';
  int z = 0;  
  for(i = 8; i < 16; ++i)
  {  
    z++;  
    total_holder[i] = bit_holder1[z];   
  }
  z = 0; 
  for(i = 16; i < 24; ++i)
  {
    z++;
    total_holder[i] = bit_holder2[z];
  }
  z = 0;
  for(i = 24; i < 32; ++i)
  {
    z++;
    total_holder[i] = bit_holder3[z];
  }
/*Finds the integer value of the binary sequence */
  int count;
  unsigned int value;
  value = 0; 
  for(count=0; count<32; count++)
  {
    if(total_holder[count] == '1')
      value = (value << 1) ^ 1;       

    else if (total_holder[count] == '0')
      value = (value << 1); 
  }

  return value;
      
}   

unsigned char inttoBin(unsigned int i1) /*represents integer in binary */
{
  char value[32];
  int  zeros; 
  for(zeros = 0; zeros < 32; ++zeros)
    value[zeros] = 0; 
  int f;  
  int g;
  int i; 
  i = 0;  
  
  for(f = 31; f >=0;f--)
  {
    g = i1 >> f;
    if(g & 1)
    {
      value[i++] ='1';
    }
    else
    {
      value[i++] = '0';
    }
  }
  if(random_color == 1)
  { 
    code1 = bintoRGB(value);
    return code1; 
  } 
  else if(random_color ==2)
  {
    code2 = bintoRGB(value);
    return code2; 
  }
  else  
  {
    code3 = bintoRGB(value);
    return code3; 
  }

}
unsigned char bintoRGB(char binary_number[]) //takes binary
/*sequence and puts it in appropriate r,b,g code */
{
  char bit_holderB[9]; 
  char bit_holderG[9];
  char bit_holderR[9];
  int i;

  int z;  
  z = 0; 
  for(i = 8; i < 16;++i)
  {
    bit_holderR[z] = binary_number[i];
    bit_holderR[8] = '\0';
    z++;
  }
  z = 0; 
  for(i = 16; i < 24; ++i)
  {
    bit_holderG[z] = binary_number[i];
    bit_holderG[8] = '\0';
    z++; 
  }
  z = 0;  
  for(i = 24; i < 32; ++i)
  { 
    bit_holderB[z] = binary_number[i];
    bit_holderB[8] - '\0';
    z++; 
  }
  int a; 
  unsigned char valueR ;
  unsigned char valueB; 
  unsigned char  valueG ;
  valueR = 0;
  valueB = 0;
  valueG = 0;
  for(a = 0; a < 8; a++)
  {
    if(bit_holderR[a] == '1')
      valueR = (valueR  << 1) ^ 1;
    else if(bit_holderR[1] == '0')
      valueR = valueR << 1;
  }
  for(a = 0; a < 8; a++)
  {
    if(bit_holderG[a] == '1')
      valueG = (valueG <<1) ^ 1;
    else if(bit_holderG[a] == '0')
      valueG = (valueG << 1);
  }  
  for(a =0; a < 8; a++)
  {
    if(bit_holderB[a] == '1')
      valueB = (valueB << 1) ^ 1;
    else if(bit_holderB[a] == '0')
      valueB = (valueB << 1);
  }  
  if(random_color == 1)
    return valueR;
  else if(random_color ==2)
    return valueG;
  else 
    return valueB;
}
/*Takes char array and represents it as unsigned int */
unsigned int arraytoInt(unsigned char array[], int size)
{
  int i;
  unsigned int value = 0;  
  for(i = 0; i < size; ++i)
  {
    if(array[i] == '\0')
      break;
    value *= 10;
    value = value + (array[i] - '0');
  }

  return value;  

}

/* Reflections: One of the only things I added is that everytime a triplet is entered,
the int i was reset to allow for multiple triplet sets. After that the rest was easy as we did the rgb already in class.
Overall this part took me about an hour.
*/
