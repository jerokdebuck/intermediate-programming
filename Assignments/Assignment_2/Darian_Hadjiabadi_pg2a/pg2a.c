   /*
    Darian Hadjiabadi
   600.120
   Assignment 1 pg2a due Fr June 7, 2013
   772-559-1911
   dhadjia1
   dhadjia1@gmail.com
   */
#include <stdio.h>
#include <math.h>

//I am just assuming that the maximum number of characters in a comment is 1//100
#define MAX_LINE 100


//subtracts vectors
void vsubtraction(float *a, float *b,float *d)
{

  int i;
  for(i = 0; i < 3; i++)
  { 
    *d = *d - *a;
    *b = *b - *a;
    d++; a++; b++;
  }
}

//computes the length of orthogonal vector then normalizes

void normalize(float * a)
{
  float length = sqrt(*a**a + *(a+1)**(a+1) + *(a+2)**(a+2));
  *a  = *a / length;
  *(a+1) = *(a+1) / length;
  *(a+2) = *(a+2)/length;
}

//computes the cross product after vector subtraction

void cross_product(float * b,float * d, float * cross)
{
  *cross = *(b+1)**(d+2) - *(b+2)**(d+1);
  cross++;
  *cross = *(b+2)**d - *b**(d+2);
  cross++;
  *cross = *b**(d+1) - *(b+1)**d;
}

int main(void)
{
  float v[128][3];
  int f[128][3];
  char comment[MAX_LINE];
  
  int i,j,k,g;
  g = 1;  
  k = 0;

  FILE *fptr;
  FILE *optr;

  for (i= 0; i < 128; i++)
  {
    for (j = 0; j < 3; j++)
    {
       v[i][j] = '\0';
       f[i][j] = '\0';
    }
  }
  for(i = 0; i < 100; i++)
    comment[i] = '\0';

  fptr = fopen("input.obj","r");
  optr = fopen("output.obj","w");
  fprintf(optr,"XVX-Darian Hadjiabadi testing\n");
 //x,y,z used to store ints and floats in v[][] and f[][]
  float x;
  float y;
  float z;
  char * c;
  char * ch;
  c = comment;
  ch = comment;

  if(fscanf(fptr,"%s %f %f %f",c,&x,&y,&z) == 1) {}
    // Will account for any comments in first lines of the .obj file
  while(*c == '#')
  { 
    if(*fgets(ch,MAX_LINE,fptr)==1){}
    if(fscanf(fptr,"%s %f %f %f",c,&x,&y,&z)==1) {}   
  }
  
 while(!feof(fptr))
 {   
  
   if (*c == 'v')
   {
    v[k][0] = x;
    v[k][1] = y;
    v[k][2] = z;
    k++;
    fprintf(optr,"%c %f %f %f\n",comment[0],x,y,z);
   }
   else if(*c == '#')
   {
     if(*fgets(ch,MAX_LINE,fptr)==1){}
   }
   else if(*c == 's')
   {
    fprintf(optr,"s off\n"); 
   }   
   else if(*c == 'f')
   {
    int q;
    q = 1;
    float a[3]; float b[3]; float d[3]; float cross_vector[3]; 
    f[g][0] = (int) x;
    f[g][1] = (int) y;
    f[g][2] = (int) z;
    g++;

    for(i = 0; i < 3; i++)
    {
      a[i] = v[(int)x-q][i];
      b[i] = v[(int)y-q][i];
      d[i] = v[(int)z-q][i];
    }
      
     float * aptr;float * bptr; float * dptr; float * crossptr; 
     aptr = a;bptr = b;dptr = d;crossptr = cross_vector; 
      
     vsubtraction(aptr,bptr,dptr);
     cross_product(bptr,dptr,crossptr);
     normalize(crossptr);

      fprintf(optr,"vn %.6f %.6f %.6f\n",cross_vector[0],cross_vector[1],cross_vector[2]);       
   }     
    if(fscanf(fptr,"%s %f %f %f",c,&x,&y,&z)==1)
   {} 
 }
// Prints f v1//n1 v2//n2 v3//n3 into output file
   int count; 
   for(count =1 ; count < g; count++)
   fprintf(optr,"f %d//%d %d//%d %d//%d\n",f[count][0],count,f[count][1],count,f[count][2],count);
   
   fclose(fptr);
   fclose(optr);
   return 0;
}

/* REFLECTION: I would say the hardest part about this 
  assignment was trying to figure out how to parse the lines
  when 's off' or an '#' came into play. Despite the struggle I think
  I found a way to do it. After that, using pointers and pointer
  arithmatic to compute the normal was not so bad. Overall this assignment 
  took me around 8 hours (mostly from the line parsing) */
