/* 
  Darian Hadjiabadi 
  Assignment 4 stability.c
  Insert and remove 10000 players!! Oh my!
*/

#include <stdbool.h>
#include <stdlib.h>
#include "player.h"
#include "sdbm.c"
#include <string.h>
#include <stdio.h>
#include <time.h>

#define MAX_LENGTH 16;

//Make sure to delete testingdb.db before running program
//Just in case!
int main(void)
{
  printf("Begin stability testing\n"); 
  if(!sdbm_open("testingdb.db"))
  {
    if (!sdbm_create("testingdb.db"))
    {
      printf("Failed to create database \n");
      return 0;
    }
    if(!sdbm_open("testingdb.db"))
    {
      printf("Failed to open database\n");
      return 0;
    }
  }
  Player * testplayers = calloc(10000,sizeof(Player));
  char alphabet[26] = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
  //Start filling database
  int i;
  for(i = 0; i < 10000; i++)
  {
    char named[16];
    int j;
    for(j = 0; j < 16; j++)
      named[j] = '\0';

    int pick=3;
   
    named[0]=named[1]=named[2] = alphabet[rand()%24+1];
    while(pick < 15)
    {
      int random = rand()%24+1;
      named[pick] = alphabet[random];
      pick++;   
    }
        
    int randhealth = rand() % 99 + 1;
    int randmana = rand() % 80; 
    int randexperience = rand() % 5000;
    float randx = rand()%500;
    float randy = rand() % 500;
    strcpy(testplayers[i].name, named);
    testplayers[i].playerID = 0;
    testplayers[i].health = randhealth;
    testplayers[i].mana = randmana;
    testplayers[i].experience = randexperience;
    testplayers[i].x = randx;
    testplayers[i].y = randy;
    sdbm_insert(named,&testplayers[i]);
  }

  sdbm_sync();


//Now remove the people from database
  for(i = 0; i < 10000; i++)
  {
    sdbm_remove(testplayers[i].name);
  }
  sdbm_sync();

  free(testplayers);
  printf("Testing done, database should now be empty!\n");
  return 0;
}
