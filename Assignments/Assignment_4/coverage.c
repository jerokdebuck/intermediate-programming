/* 
 Darian Hadjiabadi 
 Assignment 4 coverage.c
 For a lot of these functions, I am going to 
 test an instance where it will succeed and 
 an instance of failure
*/

#include <stdbool.h>
#include <stdlib.h>
#include "player.h"
#include "sdbm.c"
#include <string.h>
#include <stdio.h>

int main(void)
{
  printf("Begin testing functions\n");
  
//sdbm_create
  int x;
  x = sdbm_create("test.db");
  printf("A one will indicate create success, 0 otherwise: %d\n",x);
  sdbm_open("test.db");  
  //sdbm_has- test length
  char * test_string1 = "aa";
  char * test_string2 = "baaaa";
  char * test_string3 = "hellohellohellohellohellohel";
  sdbm_has(test_string1);
  sdbm_has(test_string2);
  sdbm_has(test_string3); 
//sdbm_insert
  Player covplayer1 = {"Generic",0,100,80,10,5,5};
  int d,e;
  d = sdbm_insert("Generic",&covplayer1);
  printf("If 1, successfully imported to memory. If 0, there already exists a player with the same name: %d\n",d);
  Player covplayer2 = {"",0,100,80,10,4,4};
  e = sdbm_insert("Generic",&covplayer2);
  printf("If 1, successfully imported. If 0, there already exists a player with the same name: %d\n",e);
 
//sdbm_put
  covplayer1.health = 80;
  int f,g;
  f = sdbm_put("Generic",&covplayer1);
  printf("If 1, successfully updated information. If 0, the player was not found so information was not updated: %d\n",f);
  g = sdbm_put("Fail",&covplayer1);
  printf("If 1, successfully updated information. If 0, the player was not found so informationw as not updated: %d\n",g);


//sdbm_get
  Player retrieveplayer;
  int h,i;
  h = sdbm_get("Generic",&retrieveplayer);
  printf("If 1, then retrieveplayer received information from database. If 0, then name could not be found and retrieveplayer could not receive the information: %d\n",h);
  i = sdbm_get("MoreFail",&retrieveplayer);
  printf("If 1, then retrieveplayer received information from database. If 0, then name could not be found and retrieveplayer could not receive the information: %d\n",i); 

 //sdbm_remove
 int j,k;
 j = sdbm_remove("Generic");
 k = sdbm_remove("Againfail");
 printf("If 1, successfully removed. If 0, name not found so no removal taken place: %d\n",j);
 printf("If 1, successfully removed. If 0, name not found so no removal taken place: %d\n",k);

//Testing sdbm_close --> also tests sdbm_sync()
//Output in test.db if you want to check  
  int m;
  Player * last =calloc(2,sizeof(Player));
  strcpy(last[0].name,"Bob");
  last[0].playerID = 0;
  last[0].health = 100;
  last[0].mana = 80;
  last[0].experience = 50;
  last[0].x = 20;
  last[0].y = 5;
  strcpy(last[1].name,"Evan");
  last[1].playerID = 1;
  last[1].health = 100;
  last[1].mana = 40;
  last[1].experience = 20;
  last[1].x = 5;
  last[1].y = 6;
  sdbm_insert("Bob",&last[0]);
  sdbm_insert("Evan",&last[1]);
  free(last); 
  m = sdbm_close();
  printf("If properly synched and closed, result is 1: %d\n",m);
   
//sdbm_open
  int q;
  q = sdbm_open("tricycles.db");
  printf("If 1, file previously existed and is open. If 0, file does not exist and was not opened: %d\n",q);
  
  FILE *fptr;
  fptr = fopen("ctest.db","w");
  fclose(fptr);
  int r;
  r = sdbm_open("ctest.db");
  printf("If 1, file previously existed and is open. If 0, file does not exist and was not opened: %d\n",r);
  sdbm_close();

//failed sdbm_create
  int t; 
  t = sdbm_create(0);
  printf("If 1, database created. Otherwise, failed: %d\n",t);
 
//read in a file that already has data
   fptr = fopen("ctest.db","w");
   fprintf(fptr,"Darian 2 100 80 10 1.2 4.3\n");
   fprintf(fptr,"Josh 1 80 85 10 1.1 2.2");
   fclose(fptr);
   sdbm_open("ctest.db");
   printf("Successfully opened and copied files");  
   sdbm_close(); 


 return 0;

}
