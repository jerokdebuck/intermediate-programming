/* 
Darian Hadjiabadi
600.120
Assignment 4 sdbm.c due June 14 2013
772 559 1911
dhadjia1
dhadjia1@gmail.com
*/
#include "player.h"
#include "sdbm.h"
#include <string.h>
#include <stdio.h>
/**
 * Minimum and maximum length of key and name strings,
 * including '\0' terminator.
 */
#define MIN_KEY_LENGTH 3
#define MAX_KEY_LENGTH 16
FILE * fptr = 0;
int array_size = 40;
static Player * database;
static int getdbase;
static int allocator = 0;



/**
 * Create new database with given name. You still have
 * to sdbm_open() the database to access it. Return true
 * on success, false on failure.
 */
bool sdbm_create(const char *name)
{    if((fptr=fopen(name,"w"))!=NULL)
     {
      fclose(fptr);
      return 1;
     }
     else
        return 0;  
}

/**
 * Open existing database with given name. Return true on
 * success, false on failure.
 */
bool sdbm_open(const char *name)
{
    if((fptr=fopen(name,"r")) == NULL)
     return 0;

     database = calloc(array_size, sizeof(Player));
    
  /*
     char * playername = malloc(strlen(name)+1);
     int id,stam,magic,xp;
     float x_pos;
     float y_pos;
     int tracker = 0;
  
     fscanf(fptr,"%s %d %d %d %d %f %f",playername,&id,&stam,&magic,&xp,&x_pos,&y_pos);
 //Read in any info in dbase to mem
     while(!feof(fptr))
      {
        
        strcpy(database[tracker].name,playername);
        database[tracker].playerID = id;
        database[tracker].health = stam;
        database[tracker].mana = magic;
        database[tracker].experience = xp;
        database[tracker].x = x_pos;
        database[tracker].y = y_pos;
        tracker++;
        if(tracker == array_size)
        {
          array_size *= 2;
          database = realloc(database,sizeof(Player)*array_size);
        }
        
      fscanf(fptr,"%s %d %d %d %d %f %f",playername,&id,&stam,&magic,&xp,&x_pos,&y_pos);
      } 
     After done reading in lines that may or may not
     have been present, close file then repon using
     write so file is now empty 
   */
     fclose(fptr);
     fptr = fopen(name,"w");
     //free(playername);
     return 1;
}

int sdbm_error()
{
   return 0;  
}
  
bool sdbm_close()
{
  /* Calls in sdbm_sync(), frees the memory, and cloes file */
  
  int x;
  x = sdbm_sync();
  if(x==1)
  {
    fclose(fptr);
    free(database);
    return 1;
  }
  else
    return 0;
}
bool sdbm_sync()
{

/* Organize in increasing playerID order
This is just in case if database existed prior,
the id's will be sorted as to specifications */ 

/* Does not work how I want it.

  int pass, i;
  char * holdName = malloc(sizeof(char));
  int holdID,holdHealth,holdMana, holdXP;
  float holdX;
  float holdY; 

  
  for(pass =1; pass <=(array_size-1); pass++)
  {
    for(i = 0; i <= (array_size-1); i++)
    {
      if(database[i].playerID> database[i+1].playerID) 
      {
        holdID = database[i].playerID;
        holdHealth = database[i].health;
        holdMana = database[i].mana;
        holdXP = database[i].experience;
        holdX = database[i].x;
        holdY = database[i].y;
        strcpy(holdName,database[i].name);
      
        strcpy(database[i].name,database[i+1].name);
        database[i].playerID = database[i+1].playerID;
        database[i].health=database[i+1].health;
        database[i].mana=database[i+1].mana;
        database[i].experience=database[i+1].experience;
        database[i].x = database[i+1].x;
        database[i].y = database[i+1].y;

        strcpy(database[i+1].name,holdName);
        database[i+1].playerID = holdID;
        database[i+1].health = holdHealth;
        database[i+1].mana = holdMana;
        database[i+1].experience = holdXP;
        database[i+1].x = holdX;
        database[i+1].y = holdY;
       }
     }
   }
 */

  /* Write from memory to database file.
  First clear out file in case sync is called before */
  
  fclose(fptr);
  //change name to specifications 
  fptr = fopen("test.db","w");

  /*Now take memory and print into database file */
  int writer;
  for(writer = 0; writer < array_size;writer++)
  {
     if(database[writer].health != '\0')
     {
      char * name = database[writer].name;
      int id = database[writer].playerID;
      int stam = database[writer].health; 
      int magic = database[writer].mana; 
      int xp = database[writer].experience;
      float x_loc = database[writer].x;
      float y_loc = database[writer].y;     
      fprintf(fptr,"%s %d %d %d %d %f %f\n",name,id,stam,magic,xp,x_loc,y_loc);    
     }
            
   }
    return 1;

}
bool sdbm_has(const char *key)
{
  //check length of key
  if((strlen(key) < (MAX_KEY_LENGTH+1)) && (strlen(key) > (MIN_KEY_LENGTH-1)))
  {
  //getdbase will be useful for inserting into database array
    for(getdbase = 0; getdbase < array_size; getdbase++)
    {
      if(strcmp(key,database[getdbase].name)==0)
        return 1;
    
    }
  
    return 0;
     
  }
  else
  {
    printf("Error, incorrect name length\n");
    return 0;
  }
}
bool sdbm_get(const char *key, Player *value)
{
  if(sdbm_has(key))
  {
   //give to value the database associated with the key

   value -> health = database[getdbase].health;
   value -> mana = database[getdbase].mana;
   value -> experience = database[getdbase].experience;
   value -> x = database[getdbase].x;
   value -> y =  database[getdbase].y; 
   return 1;
  }
  else 
    return 0;
}
bool sdbm_put(const char *key, const Player *value)
{
  //give to the database the information in value
    if(sdbm_has(key))
    {
       
       database[getdbase].health = value -> health;
       database[getdbase].mana = value -> mana;
       database[getdbase].experience = value ->experience;
       database[getdbase].x = value -> x;
       database[getdbase].y = value -> y;
       allocator++;
       return 1;
    }
    else
      return 0;
}
bool sdbm_insert(const char *key, const Player *value)
{
  if(sdbm_has(key))
    return 0;
  else
  {  
    if(allocator == array_size-1)
    {
      array_size = array_size * 2; 
      database = realloc(database,array_size*sizeof(Player));
    }
    /* Inserts value into database position */
      int j = 0; 
      while(database[j].health != '\0')
        j++;
      strcpy(database[j].name,value->name);
      database[j].playerID = value -> playerID;
      database[j].health = value -> health;
      database[j].mana = value -> mana;
      database[j].experience = value -> experience;
      database[j].x = value -> x;
      database[j].y = value -> y; 
      allocator++;
      int i;
      //Incrememnt all the playerIDs by one 
     for(i = 0; i < j; i++)
         database[j].playerID++;
     return 1;  
  }
}

bool sdbm_remove(const char *key)
{
  if(sdbm_has(key))
  {
  /* shift everything back so that database
  does not have a gap */ 
    int i;
    for(i = getdbase; i < array_size; i++)
    {
      if(database[i+1].name == '\0')
        break;
      database[i] = database[i+1];
    } 
  /* Check how full the database is. If only a
  quarter of array_size, reallocate to half */

    if(database[array_size/4].health == '\0')
    {
      array_size = array_size / 2; 
      database = realloc(database,array_size*sizeof(Player));     
    }
    return 1;
  }
  else
      return 0;
}
/* 
  Reflections: This was a doozy. It took a while for me 
to figure it out and even longer to do it. It was a 
challenging and I wasn't able to implement everything 
I wanted. Nonetheless, challenges are good every now 
and then and maybe one day I'll be able to finish this 
up. Overall this took about 12 hours */
