#ifndef _SDBM_H
#define _SDBM_H

#include <stdbool.h>

#include "player.h"

/**
 * Minimum and maximum length of key and name strings,
 * including '\0' terminator.
 */
#define MIN_KEY_LENGTH 3
#define MAX_KEY_LENGTH 16

/**
 * Create new database with given name. You still have
 * to sdbm_open() the database to access it. Return true
 * on success, false on failure.
 */
bool sdbm_create(const char *name);

/**
 * Open existing database with given name. Return true on
 * success, false on failure.
 */
bool sdbm_open(const char *name);

/**
 * Synchronize all changes in database (if any) to disk.
 * Useful if implementation caches intermediate results
 * in memory instead of writing them to disk directly.
 * Return true on success, false on failure.
 */
bool sdbm_sync();

/**
 * Close database, synchronizing changes (if any). Return
 * true on success, false on failure.
 */
bool sdbm_close();

/**
 * Return error code for last failed database operation.
 */
int sdbm_error();

/**
 * Is given key in database?
 */
bool sdbm_has(const char *key);

/**
 * Get value associated with given key in database.
 * Return true on success, false on failure.
 *
 * Precondition: sdbm_has(key)
 */
bool sdbm_get(const char *key, Player *value);

/**
 * Update value associated with given key in database
 * to given value. Return true on success, false on
 * failure.
 *
 * Precondition: sdbm_has(key)
 */
bool sdbm_put(const char *key, const Player *value);

/**
 * Insert given key and value into database as a new
 * association. Return true on success, false on
 * failure.
 *
 * Precondition: !sdbm_has(key)
 */
bool sdbm_insert(const char *key, const Player *value);

/**
 * Remove given key and associated value from database.
 * Return true on success, false on failure.
 *
 * Precondition: sdbm_has(key)
 */
bool sdbm_remove(const char *key);

#endif /* _SDBM_H */
