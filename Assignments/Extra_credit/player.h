#ifndef _PLAYER_H
#define _PLAYER_H

 struct node
{
  char * name;
  int health;
  struct node * next;
};

typedef struct node * List;
typedef struct node * Node;
#endif //_PLAYER_H
