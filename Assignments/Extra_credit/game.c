/* Ask user how many players
- group player data together into one unit
[- restrict who shoots what, add more weapons - just an idea]
*/
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "game.h"
#include "player.h"

//Global variables
int NUM_PLAYERS;

//Enums are integers
//PLAYER1 == 0, PLAYER2 == 1
enum {
    PLAYER1, PLAYER2
};        //Don't forget the semi-colon!


void addTail(char* named, List * lptr)
{
  Node np = malloc(sizeof(Node));
  np->name = named;
  np->health = 100;
  np->next = NULL;
  if(*lptr == NULL)
  {
    *lptr = np;
  }
  else
  {
    Node head = *lptr;
    while(head->next != NULL)
      head = head->next;
    head->next = np;
  }
}

int main(int argc, char *argv[])
{
    List playerlist = NULL;
    int np;

    //Make sure two values are specified
    if (argc != 2) {
  printf("Usage: game numPlayers\n");
  return 0;
    }

    NUM_PLAYERS = atoi(argv[1]);
    np = NUM_PLAYERS;
    

    printf("enter the players names\n");
    for (int i = 0; i < NUM_PLAYERS; i++) {
      char named[21];
      scanf("%s", named); 
      addTail(named,&playerlist);
    }

    int turn = -1;
//    while (!gameOver(&playerlist)) {
  turn = (turn + 1) % NUM_PLAYERS;  // roll over to 0 
  printTurn(turn,playerlist);

  //Attack and target tokens longer than 9 characters
  //will not work 
  char attack[10], target[10];
  scanf("%s %s", attack, target);

  int playerIndex = getPlayerIndex(playerlist,target);

  if (strcmp(attack, "fireball") == 0)
      fireball(turn, playerIndex,&playerlist);
  else if (strcmp(attack, "arrow") == 0)
      arrow(turn, playerIndex,&playerlist);
  else
      printf("Invalid spell!\n");

  if (!isAlive(playerIndex,&playerlist) && turn >= playerIndex)
      turn--;

   // }
/*
    printf("winner is %s!\n", players->name); // shorthand for players[0].name

    for (int i = 0; i < NUM_PLAYERS; i++)
  free(players[i].name);
    free(players);
*/
    return 0;
}

int getPlayerIndex(List head,const char *name)
{
    int i = 0;
    while(head && head ->name != name)
    {
      head = head -> next;
      i++;
      if(head->name == name)
        return i;
  }
    printf("Invalid target! Type a new player name:\n");
    char newName[10];
    scanf("%s", newName);
    return getPlayerIndex(head,newName);
}

/*
bool gameOver(List * lptr)
{
    int count = 0;
    List temp;

    for (int i = 0; i < NUM_PLAYERS; i++) {
      if (isAlive(i,lptr))
        count++;
      else {
        printf("%s is DEAD\n", (*lptr)->name);
        *lptr = (*lptr)->next;
        temp = *lptr;
        for (int j = i +1; j < NUM_PLAYERS; j++)
          //  playerlist-1 =playerlist;
  //      playerlist[--NUM_PLAYERS] = temp;
        i--;    // backup a player
  }
    }

    return count == 1;
}
*/
bool isAlive(int player,List * lptr)
{
    checkValidPlayer(player);
    int i;
    for(i = 0; i < player; i++)
      *lptr = (*lptr) -> next;
    if((*lptr)->health > 0)
      return (*lptr)->health;
}

void printTurn(int player, List person)
{
 if(person)
 { 
   printf("%s now has %d health, ", person->name,
        person->health);
   printTurn(player,person->next);
  }
  else
  {
    
    char * next_name= getPlayerName(player,&person);
    printf("\n%s's turn\n", next_name);
  }
}

void fireball(int source, int target,List * lptr)
{
    checkValidPlayer(source);
    checkValidPlayer(target);

    //Decrease target's health
    int i ;
    for(i =0; i < source; i ++)
     *lptr = (*lptr)->next;
     
    (*lptr)->health -= FIREBALL_DAMAGE;

    printf("%s cast fireball at %s, dealing %d damage\n",
     getPlayerName(source,lptr), getPlayerName(target,lptr), FIREBALL_DAMAGE);
}

void arrow(int source, int target,List * lptr)
{
    checkValidPlayer(source);
    checkValidPlayer(target);
    int i;
    for(i = 0; i<source; i++)
      *lptr = (*lptr)->next;
    //Decrease target's health
    (*lptr)->health -= ARROW_DAMAGE;

    printf("%s shot an arrow at %s, dealing %d damage\n",
     getPlayerName(source,lptr), getPlayerName(target,lptr), ARROW_DAMAGE);
}

/** Returns the player's name from the array of names base on his or her index.
*/
char *getPlayerName(int player, List * lptr)
{
    int i;
    for(i = 0; i < player; i++)
      *lptr = (*lptr)->next;
    checkValidPlayer(player);
    return (*lptr)-> name;
}

/** Checks if the player (integer) is valid. Exits the program on failure.
*/
void checkValidPlayer(int player)
{
    assert(player >= 0);
    assert(player < NUM_PLAYERS);
}
