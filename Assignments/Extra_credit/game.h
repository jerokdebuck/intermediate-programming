#ifndef _GAME_H
#define _GAME_H
#include <stdbool.h>
#include "player.h"
#define FIREBALL_DAMAGE 30
#define ARROW_DAMAGE 40

int getPlayerIndex(List,const char * name);
bool gameOver();
bool isAlive(int,List *);
void fireball(int, int,List *);
void arrow(int, int,List *);
char* getPlayerName(int, List *);
void checkValidPlayer(int);
void printTurn(int,List);
void addTail(char *,List * );
#endif //_GAME_H
