#include <string>

#ifndef COLOR_H
#define COLOR_H

using std::string;

class Color
{
  private:
    int regular;
    string hexy; 
    int r;
    int g;
    int b;
  public:
    Color();
    Color(int); 
    Color(int, int, int);
    Color(string); 
    ~Color();
    
    const Color & operator=(const Color &);
    
    int value();
    string rgb();
    string hex();
    
    int changeRed(int);
    int changeGreen(int);
    int changeBlue(int);
};
#endif //COLOR_H
