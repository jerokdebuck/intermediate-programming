/*
Darian Hadjiabadi
600.120
Assignment 6 Color.cpp
dhadjia1@gmail.com  
dhadjia1
*/

#include "Color.h"
#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>
#include <cstdlib>
//Determines which constructures have been used
using std::cout;
using std::string;

string stream(int);
int int_stream(string);

Color::Color()
{
  regular = 16777215;
  r = 255; 
  b = 255; 
  g = 255;
  hexy = "#ffffff";
}

Color::Color(int regular)
{
  this -> regular = regular; 
  this -> r = -1;
  this -> g = -1;
  this -> b = -1;
  this -> hexy = "-1";
}
Color::Color(int r, int g, int b)
{
  if(r <= -1)
    this -> r = 0;
  else if(r > 255)
    this -> r = 255;
  else
    this -> r = r;
  if(g <= -1) 
   this -> g = 0;
  else if(g > 255) 
    this -> g = 255;
  else
    this -> g = g;
  if(b <= -1) 
    this -> b = 0;
  else if(b > 255) 
    this -> b = 255;
  else
    this -> b = b;

  this -> regular = -1;
  this -> hexy = "-1";
}
Color::Color(string hexy)
{
  this -> hexy = hexy;
  this -> regular = -1;
  this -> r = -1;
  this -> g = -1;
  this -> b = -1;
}
/*
Color Color::black()
{
  this -> regular = 0;
  this -> r = 0;
  this -> g = 0;
  this -> b = 0;
  this -> hexy = "#00000000";
}
*/
int Color::value()
{
 if(regular != -1)
    return regular;
 else
 {
    if (hexy != "-1")
    { 
      hexy.at(0) ='\0';
      int n = 0;
      int k;
      unsigned int i; 
      for(i = 0; i < hexy.length(); i++)
      {
        k = hexy.at(i)-48;
        if ( k > 9)
          k-=7;
        k &= 0x0f;
        n=(n<<4) | k;
      }
      regular = n; 
      return regular;
    
    }
    else if (r != -1)
    {
      std::cout<<"ping"<<std::endl;
      int rgb = r*65536 + g*256 + b;
      return regular = rgb;
    }
          
 }
 return 0; 
}
string Color::rgb()
{
  if (r!= -1)
  {
    string Red = stream(r);
    string Green = stream(g);
    string Blue = stream (b);
    return "rgb("+Red+","+Green+","+Blue+")";
  }
  else
  {
    if (regular != -1)
    {
      int blue = regular / 65536;
      int green = (regular - (blue*65536))/256;
      int red = (regular-(blue*65536)-(green*256));
      r = blue; g = green; b = red; 
      return "rgb("+stream(r)+","+stream(g)+","+stream(b)+")";
    }
    
    else if (hexy != "-1")
    {
      unsigned int i;
      int k;
      int n = 0;
      hexy.at(0) = '\0';
      for(i = 0; i < hexy.length(); i++)
      {
        k = hexy.at(i) - 48;
        if(k > 9)
          k-=7;
        k &= 0xff;
        n = (n<<4) | k;
      }

      int blue = n / 65536;
      int green = (n - (blue*65536))/256;
      int red = (n-(blue*65536)-(green*256));
      r = blue; g = green; b = red;
      return "rgb("+stream(r)+","+stream(g)+    ","+stream(b)+")";

       

    }
  }
  return "0";
} 
string Color::hex()
{
  if(hexy != "-1" )
  {
    return hexy;
  }
  else
  {
    if(regular != -1)
    {  
      std::ostringstream oss;
      oss<<std::hex<<regular;
      string hexString = oss.str();
      int i;
      for(i = hexString.length(); i < 8; i++)
        hexString = "0" + hexString;
      return hexy ="#" + hexString;
    }
    
    else if (r != -1)
    {
      int rgb = r*65536 + g*256 + b;
      std::ostringstream iss;
      iss<<std::hex<<rgb;
      string num_string = iss.str(); 
      int i;
      for(i = num_string.length(); i < 8; i++)
        num_string = "0" + num_string;
      return hexy="#" + num_string;

    }
  }
  return "0";
    
}
int Color::changeRed(int n)
{
  int x = r + n;
  if(x > 255)
  {
    r = 255;    
    return (x - n);
  }
  else if (x < 0)
  {
    r = 0;
    return (x - n);
  }
  else
  { 
    r = x;
    return (x - n);
  }
 
}

int Color::changeBlue(int n)
{
  int x = b + n;
  if(x > 255)
  {
    b = 255;
    return (x - n);
  }
  else if(x < 0)
  {
    b = 0;
    return (x - n);
  }
  else
  {
    b = x;
    return (x - n);
  }
}

int Color::changeGreen(int n)
{
  int x = g + n;
  if(x > 255)
  {
    g = 255;
    return (x - n);
  }
  else if(x < 0)
  {
    g = 0;
    return (x - n);
  }
  else
  {
    g = x; 
    return (x - n);
  }
}

Color::~Color()
{

}

const Color & Color::operator=(const Color & fromColor)
{
  if(this != & fromColor)
  {
    r = fromColor.r;
    g = fromColor.g;
    b = fromColor.b;
    hexy = fromColor.hexy;
    regular = fromColor.regular; 
  }
  return *this;
}

string stream(int value)
{
  std::ostringstream ss;
  ss << value;
  return ss.str();
}

int int_stream(string value)
{
  int number; 
  std::istringstream(value)>>number;
  return number;
}

/* The first part of this assignment wasn't too bad. 
I did find writing the class for the second part
to be a bit easier. Overall this took me about 
4 hours */
