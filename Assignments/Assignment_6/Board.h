#include "Color.h"


#ifndef BOARD_H
#define BOARD_H
template <class T>
struct Cells
{
  T data;
  int bgcolor;
  int forcolor;
};

template <class T>
class Board
{
   private:
     int r;
     int c;
     struct Cells<T> ** pieces;
   public:
     Board();
     Board(int);
     Board(int,int);
     Board(const Board &);
     ~Board();
      
     //const Bag & operator=(const Bag &);
    
     bool setBackground(Color, int, int);
     bool setForeground(Color, int, int);
     bool setCell(T, int, int);
     void initBoard(T);
     T getCell(int,int);
     T & getCellReference(int, int);
     Board & getSub(int,int,int,int);
};

#endif
