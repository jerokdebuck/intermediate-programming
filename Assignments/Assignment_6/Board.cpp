/*
Darian Hadjiabadi
600.120
Assignment 6 Board.cpp
dhadjia1@gmail.com 
dhadjia1
*/

#include "Board.h"
#include "Color.h"
#include <iostream>
#include <string>
#include <stdexcept>
#include <cstdio>
#include <cstring>

using std::string;
using std::endl;
using std::cout; 
using std::cin;
using std::ios;
using std::cerr;

template <class T>
Board<T>::Board()
{
}

template <class T>
Board<T>::Board(int r, int c)
{
  this -> r = r;
  this -> c = c;
  int i;          
  pieces = new struct Cells<T>*[r];
  for(i = 0; i < r; i++)
  {
    pieces[i] = new struct Cells<T>[c];
  }
  int j;
  for(i = 0; i < r; i ++)
  {
    for(j = 0; j < c; j++)
    {
      pieces[i][j].bgcolor = 0xFFFFFF;
      pieces[i][j].forcolor = 0x0;
    }
  } 
}

template <class T>
Board<T>::Board(int sq)
{
  this -> r = sq;
  this -> c = sq; 
  int i;
  
  pieces = new struct Cells<T>*[r];
  for(i = 0; i < r; i++)
    pieces[i] = new struct Cells<T>[c];
  int j;
  for(i =0; i < r; i++)
  {
    for(j = 0; j < c; j++)
    {
      pieces[i][j].bgcolor = 0xFFFFFF;
      pieces[i][j].forcolor = 0x0;
    }
  } 
}

template <class T>
Board<T>::Board(const Board & fromBoard)
{
  r = fromBoard.r;
  c = fromBoard.c;
  int i;int j; 
  
  pieces = new struct Cells<T>*[r];
  for(i = 0; i < r; i++)
    pieces[i] = new struct Cells<T>[c];

  for(i = 0; i < r; i++)
  {
    for(j = 0; j < c; j++)
    {
      pieces[i][j].data = fromBoard.pieces[i][j].data;  
      pieces[i][j].bgcolor= fromBoard.pieces[i][j].bgcolor;
      pieces[i][j].forcolor = fromBoard.pieces[i][j].forcolor;
    }
  } 
}
template <class T>
Board<T>::~Board()
{
  int i;
  for(i = 0; i < r; i++)
    delete [] pieces[i];
  delete [] pieces;
}

/* 
string Board::htmlTable()
{
    


} 
*/
template <class T>
bool Board<T>::setBackground(Color color, int row, int col)
{
  if(row <= r && col <= c && row >= 0 && col >= 0)
  {
    int hexadec = color.value();
    pieces[row][col].bgcolor= hexadec;
    return true;
  }
  else
    return false;
}

template <class T>
bool Board<T>::setForeground(Color color, int row, int col)
{
  if(row <= r && col <= c && row >= 0 && col >= 0)
  {
    int hexadec = color.value();
    pieces[row][col].forcolor = hexadec;
    return true;
  }
  else 
    return false;  
}

template <class T>
bool Board<T>::setCell(T val, int row, int col)
{
  if(row <= r && col <= c && row >= 0 && col >= 0)
  {
    pieces[row][col].data = val;   
    return true;
  }
  else
    return false;
}

template <class T>
void Board<T>::initBoard(T val)
{
  int i;int j;
  for(i = 0; i < r; i++)
  {
    for(j = 0; j < c; j++)
    {
      pieces[i][j].data = val;
    }
  }
  
}

template <class T>
T Board<T>::getCell(int row, int col)
{
    if(row <= r && row >= 0 && col <= c && col >= 0)
      return pieces[row][col].data;  

    cerr << "invalid" << endl;
    return -1;
}

template <class T>
T & Board<T>::getCellReference(int row, int col)
{
    if(row <= r && row >= 0 && col <= c && col >= 0)
     return pieces[row][col].data; 
    
    cerr << "invalid" << endl; 
    return pieces[0][0].data;
}
  
template <class T>
Board<T>& Board<T>::getSub(int r1, int c1, int r2, int c2)
{
  
  if((r2 - r1) > 0 && (c2 - c1) > 0)
  {
    this -> r = r2 - r1;
    this -> c = c2 - c1;
    
    int i; int j;
    for(i = 0; i < this -> r; i++)
    {
      for(j = 0; j < this -> c; j++)
      {
        this -> pieces[i][j].data = pieces[i][j].data;
        this -> pieces[i][j].bgcolor = pieces[i][j].bgcolor;
        this -> pieces[i][j].forcolor = pieces[i][j].forcolor;
      }
    }
    return *this;
  }
  cout << "invalid" << endl;
  return *this = -1;
}    


/* This was an interesting project mainly
becasue i started it right after the Miami Heat won
the NBA championships. By using a double pointer,
I think I was able to do a majority of this part
decently. I wasn't able to get to the html table
though. Overall this took about 7 hours */
